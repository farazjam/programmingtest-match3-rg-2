# Match 3 programming assignment
Status : Complete ✔

## Game Mechanics
* The game consists of `n x n` tiles (configurable in editor or code).✔
* If three or more of the same tile type are in a horizontal row, the game removes them.✔
* A tile falls down if there is no tile below it (unless it's on the bottom row).✔
* The user can click on a tile. The game then removes the tile.✔
* When the game starts, all the positions should be filled with tiles and nothing should fall down.✔

## Requirements
* Same Unity Version `(20.3.3f1)` is used. ✔
* Do not use the physics engine to move the tiles. ✔
* Do not use raycasting for anything except if required to detect what tile the player clicked on. ✔

## Programming Approach

### Code Setup
* Text on Tile Gameobjects represent it's position, the origin is `0,0`, It's not row/column number in `tiles[,]`
* Tiles are populated visually column by column, but are inserted into tiles[,] row by row as per design decision
* In game view, the tile that is at position `8,3` is at `tiles[3,8]` in data structure
* `Board.cs` dynamically adjusts the (white) board and camera to the given grid size

### Code Structure
* `Tile.cs` is the smallest unit, is attached to a tile gameobject and handles it's type, position and fall
* `TileController.cs` manages tiles from data structure point of view including spawning and validation
* `GameController.cs` is the boss. Asks TileController to spawn tiles, handles user input, manages game states, performs a variety of match-3 operations 
* `GameController -> Tile Controller -> Tile`

### Tile Spawning
* Tiles can be auto-spawned dynamically and uniquely or can also be manually written in the editor
* `TileController.cs` handles spawning behavior and does validates tiles for being populated correctly
* For manual spawning, the `manualTiles[,]` array should be carefully edited, it can handle any nxn size data however it's users responsibility to provide unique tiles
* A match-verification check is run at start to detect any starting matches mistaklenly introduced while editing `manualTiles[,]` 
* Auto spawning instantiates game objects column by column, it generates 5 type of tiles `(Random, Unique, UniqueXX and Friendly tiles)`. Appropriate tiles are spawned to get a unique non-matching grid at the end
* `Column[0]` gets all the unique tiles, Any other column's tile will have to be careful not to spawn same tiles to achieve non-matching tiles grid
* Generating alternate unique tiles makes it sometime very hard to find any match is seems symetrical puzzle, therefore generating a 2nd tile that is same to it's adjacent left tile increases the chance of opt-in match-3 by a player's click. UniqueXX tile is spawned for a ratio of `10:1` whenever possible to generate an easy-to-play grid
* A `UniqueXX` tile s only handy if the 3rd tile on top row have same tile, so that when a user pop's the 3rd tile, the top tile fall down (which is a matching tile) w.r.t adjacent 2 tiles and get's a match 3. These tiles are called friendly tiles, that should be taken as hints, and should be clicked/popped for an easy match
* After a UniqueXX (2 adjacent matching tiles) are spawned, a similar type of 3rd tile can be spawned randomly and can spawn a match-3 tiles at start. To avoid it, any 2 adjacent tiles are detected and spawned with a non-matching tile called `Friendly tile`
* Auto-spawn always achieves non-matching grid at start with approx atleast 1 or more 2 same tiles in each row and as many as possible friendly tiles for a fun game experience

### Tile Matching
* Starting grid have no match-3 un-destroyed tiles
* At first player clicks a tile, it get's destroyed as gameobject using raycasting
* From that point, all tiles above this destroyed tile falls down `Vector3.MoveTowards` (no gravity)
* Newly falled tiles may introduce a match-3, which is checked in all rows including this and above ones
* Any newly introduced match is also destroyed, it goes to detect new match again and it keeps checking, detecting, destroying unless a non-match-3 grid is achieved.
* Game state changes appropriately effectively blocking user from click-intrupt the process and he can only click-destroy again once the detection-match-destroy cycle is complete and game is back in idle state
