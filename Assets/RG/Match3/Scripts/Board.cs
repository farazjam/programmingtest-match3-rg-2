using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public static Board instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void AdjustPosition()
    {
        float offset = (GameController.size % 2 == 0) ? 0.5f : 0f;
        this.transform.position = new Vector3((GameController.size / 2) - offset, this.transform.position.y, (GameController.size / 2) - offset);
    }
}
