using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Assertions;
using System;

[Serializable]
public enum TileType
{
    CUBE, SPHERE, CYLINDER, CAPSULE, _LENGTH
};

public class Tile : MonoBehaviour
{
    private TMP_Text tmpPositionText;
    [SerializeField]
    public TileType type;
    private bool isMovingDown;
    private Vector3 targetPosition;

    void Start()
    {
        tmpPositionText = this.gameObject.transform.GetChild(0).transform.GetComponent<TMP_Text>();
        Assert.IsNotNull(tmpPositionText.gameObject);
        isMovingDown = false;
        targetPosition = Vector3.zero;
    }

    void FixedUpdate()
    {
        tmpPositionText.text = (int)this.gameObject.transform.localPosition.x + "," + (int)this.gameObject.transform.localPosition.z;

        if (isMovingDown)
        {
            if (this.transform.localPosition == this.targetPosition)
            {
                isMovingDown = false;
            }
            else
            {
                this.transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, Time.deltaTime * 5f);
            }
        }
    }

    // Non physics based tile fall
    public void Fall()
    {
        targetPosition = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z - 1);
        isMovingDown = true;
    }
}