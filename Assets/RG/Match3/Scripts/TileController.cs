using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class TileController : MonoBehaviour
{
    public static TileController instance;
    [SerializeField]
    private GameObject[] prefabs;               // Tile prefabs, assign 4 unique from inspector
    public GameObject[,] tiles;                 // Main data structure holding all tile game objects

    // Change this grid for manual grid population along with a change of GameController.populationType = PopulationType.MANUAL
    // Your responsibility to write a non-match-3 grid but in case of a match, it will be checked in start and destroyed
    // Use a nxn size grid, rows and columns should be of same number
    // Ignore it, if generating auto grid
    TileType[,] manualTiles =
    {
        {TileType.CAPSULE, TileType.SPHERE, TileType.SPHERE, TileType.CYLINDER, TileType.CUBE , TileType.SPHERE, TileType.SPHERE, TileType.CUBE, TileType.CYLINDER, TileType.CUBE},
        {TileType.SPHERE, TileType.CYLINDER, TileType.CYLINDER, TileType.CAPSULE,TileType.CAPSULE, TileType.CYLINDER, TileType.CYLINDER, TileType.SPHERE, TileType.SPHERE, TileType.CAPSULE},
        {TileType.CUBE, TileType.SPHERE, TileType.CAPSULE, TileType.CAPSULE, TileType.CUBE, TileType.CAPSULE, TileType.CAPSULE, TileType.CUBE, TileType.CAPSULE, TileType.CUBE},
        {TileType.SPHERE, TileType.CYLINDER, TileType.CYLINDER, TileType.CUBE, TileType.CYLINDER, TileType.CUBE, TileType.CYLINDER, TileType.CUBE, TileType.SPHERE, TileType.SPHERE},
        {TileType.CAPSULE, TileType.SPHERE, TileType.CAPSULE, TileType.CUBE, TileType.CUBE, TileType.CAPSULE, TileType.SPHERE, TileType.CYLINDER, TileType.CAPSULE, TileType.CUBE},
        {TileType.SPHERE, TileType.CYLINDER, TileType.CYLINDER, TileType.CUBE, TileType.CYLINDER, TileType.CAPSULE, TileType.CAPSULE, TileType.CUBE, TileType.SPHERE, TileType.SPHERE},
        {TileType.CUBE, TileType.SPHERE, TileType.SPHERE, TileType.CUBE, TileType.CUBE, TileType.CYLINDER, TileType.CYLINDER, TileType.CUBE, TileType.CAPSULE, TileType.CUBE},
        {TileType.CUBE, TileType.CYLINDER, TileType.SPHERE, TileType.CUBE, TileType.CYLINDER, TileType.SPHERE, TileType.SPHERE, TileType.CYLINDER, TileType.SPHERE, TileType.CAPSULE},
        {TileType.CAPSULE, TileType.SPHERE, TileType.CUBE, TileType.CYLINDER, TileType.CAPSULE, TileType.CYLINDER, TileType.CAPSULE, TileType.CUBE, TileType.CAPSULE, TileType.CUBE},
        {TileType.SPHERE, TileType.CYLINDER, TileType.CYLINDER, TileType.CUBE, TileType.CUBE, TileType.CYLINDER, TileType.SPHERE, TileType.CUBE, TileType.SPHERE, TileType.CYLINDER}
    };

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public void Spawn()
    {
        if (isValidPrefabs())
        {
            if (GameController.spawnType == SpawnType.AUTO)
            {
                if (IsValid_AutoGrid())
                {
                    StartCoroutine(AutoSpawn());
                }
            }
            else
            {
                if (isValid_ManualGrid())
                {
                    StartCoroutine(ManualSpawn());
                }
            }
            Board.instance.AdjustPosition();
        }
    }

    private bool IsValid_AutoGrid()
    {
        bool result = false;
        if (GameController.size >= GameController.minsize && GameController.size <= GameController.maxsize)
        {
            result = true;
        }
        else
        {
            throw new Exception("Valid auto grid size is [3 - 10]");
        }
        return result;
    }

    private bool isValid_ManualGrid()
    {
        bool result = false;
        if (manualTiles.GetLength(0) == manualTiles.GetLength(1))
        {
            GameController.size = manualTiles.GetLength(0);
            if (GameController.size >= GameController.minsize && GameController.size <= GameController.maxsize)
            {
                result = true;
            }
            else
            {
                throw new Exception("Valid manual grid size is [3 - 10]");
            }
        }
        else
        {
            throw new Exception("Valid manual grid should have same no. of rows and columns");
        }
        return result;
    }

    private bool isValidPrefabs()
    {
        bool result = false;
        if (prefabs != null && prefabs.Length == (int)TileType._LENGTH)
        {
            for (int i = 0; i < (int)TileType._LENGTH; i++)
            {
                for (int j = 0; j < (int)TileType._LENGTH - 1; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    if (prefabs[i].GetComponent<Tile>().type != prefabs[j].GetComponent<Tile>().type)
                    {
                        result = true;
                    }
                    else
                    {
                        throw new Exception("Use unique tile prefabs, duplicate found at " + (j + 1));
                    }
                }
            }
        }
        else
        {
            throw new Exception("Use atleast " + (int)TileType._LENGTH + " unique tile prefabs");
        }
        return result;
    }

    public TileType GetTile()
    {
        return prefabs[UnityEngine.Random.Range(0, prefabs.Length)].gameObject.GetComponent<Tile>().type;
    }

    public TileType GetTile(TileType type)
    {
        return prefabs[(int)type].gameObject.GetComponent<Tile>().type;
    }

    public TileType GetTileUnique(TileType type)
    {
        // Tile type of adjacent left tile is received
        return (TileType)(((int)type + 1) % (int)TileType._LENGTH);
    }

    private IEnumerator AutoSpawn()
    {
        tiles = new GameObject[GameController.size, GameController.size];
        int twoInARowWeight = 10;
        for (int row = 0; row < GameController.size; row++)
        {
            bool skipUniqueTile = false;
            for (int column = 0; column < GameController.size; column++)
            {
                TileType tileType;
                if (column <= 0)
                {
                    // Random tile
                    tileType = GetTile();
                    Debug.Log("Random Tile");
                }
                else
                {
                    if (UnityEngine.Random.Range(0, twoInARowWeight) > 0 && skipUniqueTile == false)
                    {
                        // Random tile with weight to achieve lots of 2 same tiles in a row for playfulness
                        tileType = GetTile();
                        skipUniqueTile = true;
                        Debug.Log("Unique XX Tile");
                    }
                    else
                    {
                        if ((row > 0 && column > 1) &&
                            (tiles[row - 1, column - 1].gameObject.GetComponent<Tile>().type == tiles[row - 1, column - 2].gameObject.GetComponent<Tile>().type) &&
                            (tiles[row - 1, column - 1].gameObject.GetComponent<Tile>().type != tiles[row, column - 1].gameObject.GetComponent<Tile>().type))
                        {
                            // Friendly tile
                            // Two tiles underneath left are confirmed same
                            // Now if adjacent left is not-same, it's safe to assign bottom left tile type as this tile type
                            tileType = GetTile(tiles[row - 1, column - 1].gameObject.GetComponent<Tile>().type);
                            Debug.Log("Friendly Tile at " + column + "," + (row - 1));
                        }
                        else
                        {
                            // Unique tile
                            TileType uniqueTileType = GetTileUnique(tiles[row, column - 1].gameObject.GetComponent<Tile>().type);
                            tileType = GetTile(uniqueTileType);
                            Debug.Log("Unique Tile");
                        }
                        skipUniqueTile = false;
                    }
                }
                GameObject tile = Instantiate(prefabs[(int)tileType], this.gameObject.transform);
                tile.transform.localPosition = new Vector3(column, 0, row);
                tiles[row, column] = tile;
            }
            Debug.Log("---");
        }

        yield return null;
    }

    private IEnumerator ManualSpawn()
    {
        tiles = new GameObject[GameController.size, GameController.size];

        for (int row = 0; row < GameController.size; row++)
        {
            for (int column = 0; column < GameController.size; column++)
            {
                GameObject tile = Instantiate(prefabs[(int)manualTiles[row, column]], this.gameObject.transform);
                tile.transform.localPosition = new Vector3(column, 0, row);
                tiles[row, column] = tile;
            }
        }

        yield return null;
    }

    public void PrintListOfList()
    {
        for (int column = 0; column < GameController.size; column++)
        {
            int count = 0;
            for (int row = 0; row < GameController.size; row++)
            {
                if (tiles[column, row] != null)
                {
                    count++;
                }
            }
            Debug.Log(count);
        }
    }
}
