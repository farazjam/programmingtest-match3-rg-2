using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public enum State
{
    IDLE, MATCH, DESTROY, REFILL
};

[Serializable]
public enum SpawnType
{
    AUTO, MANUAL
};

public class GameController : MonoBehaviour
{
    public static GameController instance;
    private State state;

    public const SpawnType spawnType = SpawnType.AUTO;                      // Change this for a manual spawning, see array in TileController
    public static int size = 10;                                            // Not constant as it may need to change according to array for manual population
    public const int minsize = 3;                                           // No point in have a 2x2 game and a grid larger than 10x10 may become too much of a small tile for mobile
    public const int maxsize = 10;

    private int currentRow;
    private const float delay = 0.1f;
    private bool noMoreMatches;
    private RaycastHit hit;
    private List<List<int>> allMatchedTiles_Indexes;

    void Awake()
    {
        MakeInstance();
    }

    private void MakeInstance()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    void Start()
    {
        state = State.IDLE;
        currentRow = 0;
        noMoreMatches = false;
        allMatchedTiles_Indexes = new List<List<int>>();

        TileController.instance.Spawn();

        // Check if there already exists any match-3s, shouldn't be any
        StartCoroutine(CheckMatchFromGivenRow(0));
        state = State.IDLE;
    }

    void Update()
    {
        if (state == State.IDLE && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 50f))
            {
                Debug.Log("Player clicked " + hit.collider.name + " at [" + (int)hit.collider.transform.localPosition.x + "," + (int)hit.collider.transform.localPosition.z + "]");
                Destroy(hit.collider.gameObject);

                // A tile that is on position 3,8 is as a gameobject will be found at 8th row and 3rd column in data structure, therefore swapped indexes
                StartCoroutine(FillAfterPlayerClickDestroy((int)hit.collider.transform.localPosition.z, (int)hit.collider.transform.localPosition.x));
            }
        }
    }

    private IEnumerator FillAfterPlayerClickDestroy(int row, int column)
    {
        // Lock user input early
        state = State.MATCH;

        if (row >= 0 && row + 1 < size)
        {
            for (int i = row; i < size; i++)
            {
                if (i + 1 < size && TileController.instance.tiles[i + 1, column] != null)
                {
                    TileController.instance.tiles[i + 1, column].GetComponent<Tile>().Fall();
                    TileController.instance.tiles[i, column] = TileController.instance.tiles[i + 1, column];
                    TileController.instance.tiles[i + 1, column] = null;
                }
            }
        }

        if (TileController.instance.tiles[row, column] != null)
        {
            yield return new WaitForSeconds(0.25f);
            yield return StartCoroutine(CheckMatchFromGivenRow(row));
        }

        // Release user input lock
        state = State.IDLE;
    }

    private IEnumerator CheckMatchFromGivenRow(int row)
    {
        currentRow = row;
        noMoreMatches = false;
        while (currentRow < size)
        {

            state = State.MATCH;
            yield return StartCoroutine(FindMatchinARow());
            yield return new WaitForSeconds(delay);

            if (noMoreMatches)
            {
                break;
            }
        }

        noMoreMatches = false;
        Debug.Log("All Rows Done");
        yield return null;
    }

    private IEnumerator FindMatchinARow()
    {
        Debug.Log("Row " + currentRow + " ???");

        allMatchedTiles_Indexes.Clear();
        int allMatchedTilesIndex = -1;
        int index = 2;
        GameObject lastTile = TileController.instance.tiles[currentRow, index - 2];
        GameObject secondLastTile = TileController.instance.tiles[currentRow, index - 1];

        bool match3Stream = false;
        for (int i = index; i < size; i++)
        {
                if ((lastTile && secondLastTile && TileController.instance.tiles[currentRow, i]) 
                && (lastTile.GetComponent<Tile>().type == secondLastTile.GetComponent<Tile>().type
                    && secondLastTile.GetComponent<Tile>().type == TileController.instance.tiles[currentRow, i].GetComponent<Tile>().type))
                {
                    if (!match3Stream)
                    {
                        match3Stream = true;
                        state = State.MATCH;
                        allMatchedTilesIndex++;
                        allMatchedTiles_Indexes.Add(new List<int>() { i, i - 1, i - 2 });
                    }
                    else
                    {
                        // A match 3 is already found, list already created, this is the 3rd+ matched tile, add to list...
                        allMatchedTiles_Indexes[allMatchedTilesIndex].Add(i);
                    }
                }
                else
                {
                    match3Stream = false;
                }

                // move match pointers forward
                lastTile = TileController.instance.tiles[currentRow, (i + 1) - 1];
                secondLastTile = TileController.instance.tiles[currentRow, i - 1];
        }

        if (allMatchedTiles_Indexes.Count > 0)
        {
            Debug.Log("Matches found in row " + currentRow + " ==> " + allMatchedTiles_Indexes.Count + ", Destructing...");
            yield return StartCoroutine(DestroyMatchListInARow());
        }
        else
        {
            allMatchedTiles_Indexes.Clear();
            Debug.Log("Row " + currentRow + " Done");

            if (currentRow + 1 < size)
            {
                currentRow++;
            }
            else
            {
                noMoreMatches = true;
                Debug.Log("No more matches (STOP!!!)");
                state = State.IDLE;
                yield return null;
            }
        }
    }

    IEnumerator DestroyMatchListInARow()
    {
        yield return new WaitForSeconds(delay);
        state = State.DESTROY;

        foreach (List<int> tileIndexList in allMatchedTiles_Indexes)
        {
            foreach (int tileIndex in tileIndexList)
            {
                Destroy(TileController.instance.tiles[currentRow, tileIndex]);
            }
        }

        Debug.Log("Destruction complete for row " + currentRow + ", Refilling row " + currentRow + " to top...");
        for (int i = currentRow; i < size; i++)
        {
            yield return StartCoroutine(RefillDestroyedTilesInARow(i));
        }
    }

    IEnumerator RefillDestroyedTilesInARow(int currentRow)
    {
        yield return new WaitForSeconds(delay);
        state = State.REFILL;
        foreach (List<int> tileIndexesList in allMatchedTiles_Indexes)
        {
            if (tileIndexesList.Count > 0)
            {
                foreach (int tileIndex in tileIndexesList)
                {
                    if (currentRow + 1 < size)
                    {
                        if (TileController.instance.tiles[currentRow + 1, tileIndex] != null)
                        {
                            TileController.instance.tiles[currentRow + 1, tileIndex].GetComponent<Tile>().Fall();
                            TileController.instance.tiles[currentRow, tileIndex] = TileController.instance.tiles[currentRow + 1, tileIndex];
                            TileController.instance.tiles[currentRow + 1, tileIndex] = null;
                        }
                    }
                }
            }
        }
    }

    void OnGUI()
    {
        GUIStyle labelStyle = new GUIStyle();
        labelStyle.fontSize = 24;
        labelStyle.normal.textColor = Color.white;

        GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
        buttonStyle.fontSize = 24;
        buttonStyle.normal.textColor = Color.white;

        GUI.Label(new Rect(10, 10, 300, 50), "State = " + state, labelStyle);
        GUI.Label(new Rect(10, 50, 300, 50), "Spawn Type = " + spawnType, labelStyle);

        if (GUI.Button(new Rect(10, 100, 300, 50), "Print GameObjects[,]", buttonStyle))
        {
            TileController.instance.PrintListOfList();
        }

        if (GUI.Button(new Rect(10, 150, 300, 50), "Verify Matches", buttonStyle))
        {
            StartCoroutine(CheckMatchFromGivenRow(0));
        }
    }
}